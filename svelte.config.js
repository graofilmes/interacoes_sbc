import autoprefixer from 'autoprefixer'
import preprocess from "svelte-preprocess"
import staticAdapter from '@sveltejs/adapter-static'

/** @type {import('@sveltejs/kit').Config} */
const config = {
    kit: {
      adapter: staticAdapter({
        fallback: '404.html'
      }),
      amp: false,
      browser: {
        router: true
      },
      paths: {
        assets: '',
        base: ''
      },
      trailingSlash: 'never',
    },
    preprocess: preprocess({
      postcss: {
        plugins: [autoprefixer]
      },
    })
}

export default config
