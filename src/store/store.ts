  import { writable } from 'svelte/store';

	export const selectedPaths = writable({} as Record<string, number[]>)