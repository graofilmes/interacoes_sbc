export default [
  {
    id: 0,
    type: 'ase',
    videoId: "656974968",
    name: '<span>Mãe Luizinha</span><span>&nbsp;de Nanã</span>',
    group: 'Asé Batistini',
    color: 'sky',
    route: 'ase'
  },
  {
    id: 1,
    type: 'afoxe',
    videoId: "656973924",
    name: 'Gracinda Silva',
    group: '<span>Bloco de Afoxé</span><span>&nbsp;Omo Aiye Sele</span>',
    color: 'red',
    route: 'afoxe'
  },
  {
    id: 2,
    type: 'bloco',
    videoId: "656976366",
    name: 'Cibele Mateus',
    group: 'Bloco Maria Fuá',
    color: 'rose',
    route: 'bloco'
  },
  {
    id: 3,
    type: 'congada',
    videoId: "656977563",
    name: 'Ditinho da Congada',
    group: '<span>Grupo de Congada do</span><span>&nbsp;Parque São Bernardo</span>',
    color: 'slate',
    route: 'congada'
  },
  {
    id: 4,
    type: 'folia',
    videoId: "656979124",
    name: '<span>Pedro Baldoíno e&nbsp;</span><span>Jerede Figueiredo Gomes</span>',
    group: '<span>Folia de Reis</span><span>&nbsp;Baeta Neves</span>',
    color: 'stone',
    route: 'folia'
  },
  {
    id: 5,
    type: 'quadrilha',
    videoId: "656980180",
    name: 'Luiz Marotti',
    group: '<span class="whitespace-nowrap">Quadrilha Junina</span>&nbsp;<span class="whitespace-nowrap">do Alameda Glória</span>',
    color: 'green',
    route: 'quadrilha'
  },
  {
    id: 6,
    type: 'samba',
    videoId: "656980834",
    name: 'Washington Arantes',
    group: '<span>Escola de Samba</span><span>&nbsp;União das Vilas</span>',
    color: 'pink',
    route: 'samba'
  },
  {
    id: 7,
    type: 'viola',
    videoId: "656982082",
    name: 'Leandro de Abreu',
    group: '<span>Orquestra de Viola Caipira&nbsp;</span><span>de São Bernardo do Campo</span>',
    color: 'amber',
    route: 'viola'
  }
]