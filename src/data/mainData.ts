import type { MainData } from '../types/dataTypes'
import pathsData from "./paths"
import sectionsData from "./sections"

const mainData: MainData = {
  pathList: pathsData,
  sectionList: sectionsData
}

export default mainData