import aseData from './sections/ase.json'
import afoxeData from './sections/afoxe.json'
import blocoData from './sections/bloco.json'
import congadaData from './sections/congada.json'
import foliaData from './sections/folia.json'
import quadrilhaData from './sections/quadrilha.json'
import sambaData from './sections/samba.json'
import violaData from './sections/viola.json'

export default {
  ase: aseData,
  afoxe: afoxeData,
  bloco: blocoData,
  congada: congadaData,
  folia: foliaData,
  quadrilha: quadrilhaData,
  samba: sambaData,
  viola: violaData
}