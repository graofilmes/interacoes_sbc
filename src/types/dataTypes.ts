export interface MainData {
  pathList: Path[]
  sectionList: Record<string, Section[]>
}

export interface Path {
  id: number
  type: string
  videoId: string
  name: string
  group: string
  color: string
  route: string
}

export interface Section {
  id: number
  time: string[]
  title: string
  questions: Array<number[]>
}