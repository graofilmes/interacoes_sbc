declare module '@vimeo/player'

declare namespace svelte.JSX {
  interface HTMLAttributes<T> {
    onswipe: () => void
  }
}